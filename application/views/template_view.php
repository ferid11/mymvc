<html>

<head>
  <title>Test</title>
  <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css" />
  <link rel="stylesheet" href="<?php echo URL; ?>public/css/login.css" />
  <link rel="stylesheet" type="text/css" href="login.css">
  <link rel="stylesheet" type="text/css" href="login/login.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">





</head>
<body>
  <?php Session::init(); ?>

  <div id="templatemo_wrapper_outer">
    <div id="templatemo_wrapper_inner">

      <div id="templatemo_banner"></div>

      <div id="templatemo_menu">
        <ul>
          <?php if (Session::get('loggedIn') == true): ?>
            <?php include 'application/views/'.$head_content_view; ?>
          <?php elseif (Session::get('admin') == true): ?>
            <?php include 'application/views/admin_view/'.$head_content_view; ?>
          <?php else:  ?>
            <li><a href="<?php echo URL; ?>contacts">Əlaqə</a></li>
            <li><a href="<?php echo URL; ?>about">Haqqımızda</a></li>
            <li><a href="<?php echo URL; ?>main">Ana səhifə</a></li>

          <?php endif; ?>
        </ul>
      </div> <!-- end of menu -->

      <div id="templatemo_content_wrapper">

        <!-- sol hissenin bawlangici -->
        <div class="templatemo_side_bar margin_right_10">

          <?php if (Session::get('loggedIn') == true): ?>
            <div class="header_01">Welcome <?php echo Session::userName(); ?></div>
            <?php
            $profPic = URL."application/views/profile_pics/".Model_Set_Picture::setPicture();
            ?>
            <img src="<?php echo $profPic ?>" alt="image">
            <p>  Status: aktiv</p>
            <p> <a href="personal_task">Yerine yetirilmeyen task sayı: <?php echo Model_Task_Count::getTasksNumber(); ?></a></p>
            <p> <a href="<?php echo URL; ?>picture/index">Profil şəklini dəyiş</a></p>
            <p> <a href="<?php echo URL; ?>logout">Profili bagla</a></p>
            <br />
          <?php elseif (Session::get('admin') == true): ?>
            <div class="header_01">Welcome <?php echo Session::userName(); ?></div>
            <?php
            $profPic = URL."application/views/profile_pics/".Model_Set_Picture::setPicture();
            ?>
            <img src="<?php echo $profPic ?>" alt="image">
            <p>  Status: aktiv</p>
            <p> <a href="<?php echo URL; ?>picture/index">Profil şəklini dəyiş</a></p>
            <p> <a href="<?php echo URL; ?>logout">Profili bagla</a></p>
            <br />
          <?php else:  ?>




            <input type='checkbox' id='form-switch'>

            <form id='login-form' action="<?php echo URL; ?>login/index" method='post'>
              <input type="text" placeholder="Username" name="login" required>
              <input type="password" placeholder="Password" name="password" required>
              <button type='submit'>Login</button>
              <label for='form-switch'><span>Register</span></label>
            </form>
            <form id='register-form' action="<?php echo URL; ?>registration/index" method='post'>
              <input name="login" type="text" placeholder="Username" required>
              <input name="email" type="email" placeholder="Email" required>
              <input name='password' type="password" placeholder="Password" required>
              <input name='passwordRE' type="password" placeholder="Re Password" required>
              <button type='submit'>Register</button>
              <label for='form-switch'>Geriyə</label>
            </form>





            <div class="margin_bottom_20 horizontal_divider"></div>
            <div class="margin_bottom_20"></div>
          <?php endif; ?>
          <br /><br /><br />
          <div class="margin_bottom_20"></div>
        </div> <!-- end of left side bar -->

        <!-- content start -->
        <div class="templatemo_content margin_right_10">
          <?php if (Session::get('loggedIn') == true):
            include 'application/views/'.$content_view; ?>

          <?php elseif(Session::get('admin') == true):

            if(file_exists('application/views/admin_view/'.$content_view)){
              include 'application/views/admin_view/'.$content_view;
            }
            else{
              include 'application/views/'.$content_view;
            }
             ?>
            <br /><br /><br />
          <?php else:  ?>

            <?php include 'application/views/'.$content_view; ?>
            <div class="margin_bottom_20 horizontal_divider"></div>
            <div class="margin_bottom_20"></div>
            <div class="margin_bottom_40"></div>
          <?php endif; ?>
        </div> <!-- end of content -->

        <div class="templatemo_side_bar">
          <div>

            <h3 align="center">Təsadüfi aforizm</h3>
            <?php
            $i = rand(0,4);
            switch ($i) {
              case 0: ?>
              <p class="q">
                «Müştərinizə nə istədiyini soruşub ona görə məhsul çıxara bilməzsiniz, çünki siz onların istədiyini edərkən onlar başqa bir şey istəyəcək. (Steve Jobs)»
              </p>
              <?php    break;     ?>
              <?php   case 1: ?>
              <p class="q">
                «Biz yalnız, əməliyyat sistemlərinə baxdıq və "Bunu daha sadə və daha güclü edə bilərikmi?"-dedik. (Steve Jobs)»
              </p>
              <?php    break;     ?>
              <?php   case 2: ?>
              <p class="q">
                «Bir proyekt ortaya çıxaranda başlanğıcda səhvlər edə bilərsiniz, əhəmiyyətli olan səhvlərinizi müdafiə etmək yerinə tez bir zamanda düzəldə bilməyə çalışmaqdır. (Steve Jobs)»
              </p>
              <?php    break;     ?>
              <?php   case 3: ?>
              <p class="q">
                «Pirat olmağa qərar verdinsə, dəniz qüvvətlərinə qatılmaq məntiqsizdir. (Steve Jobs)»
              </p>
              <?php    break;     ?>
              <?php   case 4: ?>
              <p class="q">
                «Həyatımızla əlaqədar seçimlər bizə aiddir, buna görə ən yaxşısı olmaq üçün çalışmalıyıq. (Steve Jobs)»
              </p>
              <?php    break;
            }
            ?>


            <br />
            <div class="header_01">Təklif və şikayətlərinizi bildirin</div>
            <form  action="<?php echo URL; ?>suggest/index" method='post'>
              <textarea style="border: 1px solid black;" name="add_Suggest" rows="10" cols="22" placeholder="Mətni buraya daxil edin..."></textarea>
              <br>
              <!-- <input type="submit" > -->
              <input type="image" src="<?php echo URL; ?>application/views/images/send-button.jpg"
              alt="Submit" style="width:175px;padding: 10px 0px; height:40px;border:0px;" />
            </form>


            <div class="margin_bottom_20"></div>

            <!-- <div class="header_01">Sorgu</div>
            <p>
            Gununuz nece kecdi
          </p> -->



          <!-- <div class="margin_bottom_10"></div> -->
        </div> <!-- end of right side bar -->

        <div class="cleaner"></div>
      </div> <!-- end of content wrapper -->

    </div>

  </div>
  <br />  <br />  <br />
  <div class="margin_bottom_20 horizontal_divider"></div>
  <!-- <div class="margin_bottom_20"></div>
  <div class="margin_bottom_40"></div> -->
  <div align="right" id="templatemo_footer">

    <h3>(c)2018. Media invest</h3>
  </div>
  <div>
  </body>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="<?php echo URL; ?>public/js/custom.js"></script>
  </html>
