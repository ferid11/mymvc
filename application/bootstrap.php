<?php
require_once 'config/database.php';
require_once 'config/paths.php';
require_once 'core/Guardian.php';
spl_autoload_register(function ($class){
  if(file_exists(__DIR__.'/models/'.$class.'.php')){
    include_once __DIR__.'/models/'.$class.'.php';
  }
  if(file_exists(__DIR__.'/core/'.$class.'.php')){
    include_once __DIR__.'/core/'.$class.'.php';
  }
  if(file_exists(__DIR__.'/config/'.$class.'.php')){
    require_once __DIR__.'/config/'.$class.'.php';
  }
  if(file_exists(__DIR__.'/controllers/'.$class.'.php')){
    require_once __DIR__.'/controllers/'.$class.'.php';
  }
});

// require_once 'core/Session.php';
// require_once 'core/model.php';
// require_once 'core/view.php';
// require_once 'core/controller.php';
// require_once 'core/Database.php';
//
//
//
//
// include_once 'models/Model_User_Task_Count.php';
// include_once 'models/Model_Profile_Picture.php';
// include_once 'models/Model_Set_Picture.php';
// include_once 'models/Model_Disable_Task.php';
// include_once 'models/Model_Task_Count.php';
// include_once 'models/model_user_list.php';




// require_once 'core/route.php';
Route::start();
