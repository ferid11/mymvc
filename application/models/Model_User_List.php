<?php

class Model_User_List extends Model
{
  public function userList($login)
  {
    $true = 0;
    $sql = 'SELECT task_id,task,deadline from Tasks where login=? && status = ?';
    $conn=parent::__construct();
    if($stmt   =mysqli_prepare($conn,$sql)){
      mysqli_stmt_bind_param($stmt,'si',$login,$true);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_store_result($stmt);
      mysqli_stmt_bind_result($stmt,$id,$task,$deadline);

      $array =[];

      while (mysqli_stmt_fetch($stmt)) {
        $array[]=array('ad'=>$login,'id'=>$id,'task'=>$task,'deadline'=>$deadline);
        // printf ("%s (%s)\n", $login, $task);
      }
    }
    return $array;
  }
  public function onlyUsers()
  {
    $sql = 'SELECT login from Users ';
    $conn=parent::__construct();
    if($stmt   =mysqli_prepare($conn,$sql)){
      mysqli_stmt_execute($stmt);
      mysqli_stmt_store_result($stmt);
      mysqli_stmt_bind_result($stmt,$login);
      mysqli_stmt_fetch($stmt);
      $array =[];
      while (mysqli_stmt_fetch($stmt)) {
        $array[]=array('ad'=>$login);

        // printf ("%s \n", $login);
      }
    }
    return $array;
  }


}



// $sql = 'SELECT Users.login, Tasks.task from Users left join Tasks
// on Users.login = Tasks.login order by Users.id';
// $conn=parent::__construct();
// if($stmt   =mysqli_prepare($conn,$sql)){
//   // mysqli_stmt_bind_param($stmt,'s',$_POST['add_Suggest']);
//   mysqli_stmt_execute($stmt);
//   mysqli_stmt_store_result($stmt);
//   mysqli_stmt_bind_result($stmt,$login,$task);
//   mysqli_stmt_fetch($stmt);
//   $array =[];
//   while (mysqli_stmt_fetch($stmt)) {
//     $array[]=array('ad'=>$login,'task'=>$task);
//
//     // printf ("%s (%s)\n", $login, $task);
//   }
// }
// return $array;
