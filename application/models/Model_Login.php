<?php

class Model_Login extends Model
{

  public function getLogged()
  {
    if(isset($_POST['login'])){
      $login=$_POST['login'];
      $password=md5($_POST['password']);
    }
    else {
      return false;
    }

    $sql = 'SELECT id from users where login=? && password=?';
    $conn=parent::__construct();
    if($stmt   =mysqli_prepare($conn,$sql)){
      mysqli_stmt_bind_param($stmt, 'ss',$login,$password);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_store_result($stmt);
      mysqli_stmt_bind_result($stmt,$id);
      mysqli_stmt_fetch($stmt);
    }

    $count = $stmt->num_rows;
    if($count>0){
      Session::init();

      if($id==1){
        Session::set('admin',true);
      }
      else{
        Session::set('loggedIn',true);
      }

      return true;
      // header('location: ../dashboard');
    }
    else{
      return false;
      // header('location: ../login');
    }


  }

}
