<?php

class Model_Personal_Archive extends Model
{

  public function personalArchive()
  {
    $name=Session::userName();
    $true=1;

    $sql = 'SELECT task_id,task,status,deadline from tasks where login=? && status=?';
    $conn=parent::__construct();
    if($stmt   =mysqli_prepare($conn,$sql)){
      mysqli_stmt_bind_param($stmt,'si',$name,$true);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_store_result($stmt);
      mysqli_stmt_bind_result($stmt,$id,$task,$status,$deadline);
      $array =[];
      while (mysqli_stmt_fetch($stmt)) {
        $array[]=array('ad'=>$name,'id'=>$id,'task'=>$task,'status'=>$status,'deadline'=>$deadline);
        // printf ("%s (%s)\n", $login, $task);
      }
      if (mysqli_stmt_errno($stmt))
      {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }
    return $array;
  }
}
