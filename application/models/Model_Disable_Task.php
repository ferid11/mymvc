<?php

class Model_Disable_Task extends Model
{

  public function disableTask()
  {
    $box=$_POST['tasks'];
    $true=1;

    $sql = 'UPDATE tasks set status=? where task_id=?';
    $conn=parent::__construct();
    if($stmt   =mysqli_prepare($conn,$sql)){
      foreach($box as $all){
        mysqli_stmt_bind_param($stmt,'ii',$true,$all);
        mysqli_stmt_execute($stmt);
      }
      if (mysqli_stmt_errno($stmt))
      {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }
  }
}
