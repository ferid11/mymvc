<?php

class Model_Personal_Task extends Model
{

  public function userPersonal()
  {
    $login = Session::userName();
    $status=0;
    $sql = 'SELECT task_id,task,status,deadline from Tasks where login=? && status=?';
    $conn=parent::__construct();
    if($stmt   =mysqli_prepare($conn,$sql)){
      mysqli_stmt_bind_param($stmt,'si',$login,$status);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_store_result($stmt);
      mysqli_stmt_bind_result($stmt,$id,$task,$status,$deadline);

      $array =[];
      while (mysqli_stmt_fetch($stmt)) {
        $array[]=array('ad'=>$login,'id'=>$id,'task'=>$task,'status'=>$status,'deadline'=>$deadline);
        // printf ("%s (%s)\n", $login, $task);
      }
    }
    return $array;
  }


}
