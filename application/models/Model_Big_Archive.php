<?php

class Model_Big_Archive extends Model
{

  public function bigArchive()
  {
    $true=1;

    $sql = 'SELECT task_id,login,task,status,deadline from tasks where status=?';
    $conn=parent::__construct();
    if($stmt   =mysqli_prepare($conn,$sql)){
      mysqli_stmt_bind_param($stmt,'i',$true);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_store_result($stmt);
      mysqli_stmt_bind_result($stmt,$id,$login,$task,$status,$deadline);
      $array =[];
      while (mysqli_stmt_fetch($stmt)) {
        $array[]=array('ad'=>$login,'id'=>$id,'task'=>$task,'status'=>$status,'deadline'=>$deadline);
        // printf ("%s (%s)\n", $login, $task);
      }
      if (mysqli_stmt_errno($stmt))
      {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }

    return $array;
  }
}
