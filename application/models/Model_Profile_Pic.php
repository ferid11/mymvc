<?php
class Model_Profile_Pic extends Model
{


  public function uploadPicture()
  {
    //Upload to server
    $target_dir = __DIR__."/../views/profile_pics/";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $filename = pathinfo($_FILES['image']['name'], PATHINFO_FILENAME);
    $fullname=$filename.".".$imageFileType;
    move_uploaded_file($_FILES['image']['tmp_name'], $target_dir.$fullname);

    //Write name of file to DB
    $name=Session::userName();
    $sql = 'UPDATE users set image=? where login=?';
    $conn=parent::__construct();
    if($stmt   =mysqli_prepare($conn,$sql)){
      mysqli_stmt_bind_param($stmt,'ss',$fullname,$name);
      mysqli_stmt_execute($stmt);
    }
    return $fullname;
  }

}
