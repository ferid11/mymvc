<?php

class Session
{

  public static function setData($login,$password,$passwordRe = null,$mail = null)
  {
    if(isset($passwordRe) && isset($mail)){
      $array = array('login' => $login,'password'=>$password,'passwordRe'=>$passwordRe,'mail'=>$mail);
      $_SESSION['registation'] = $array;
    }
    else{
      $array = array('login' => $login,'password'=>$password);
      $_SESSION['login'] = $array;
    }
  }

  public static function init()
  {
    @session_start();
  }
  public static function userName()
  {
    // echo $_POST['login'];
    if(isset($_POST['login'])){
      $_SESSION['userName']=$_POST['login'];
    }
    return $_SESSION['userName'];
  }

  public static function set($key,$value)
  {
    $_SESSION[$key]=$value;
    // echo $_SESSION['loggedIn'];
  }

  public static function get($key)
  {
    if(isset($_SESSION[$key])){
      return $_SESSION[$key];
    }
  }
  // public static function adminSet($key,$value)
  // {
  //   $_SESSION[$key]=$value;
  // }
  // public static function adminGet($key)
  // {
  //   if(isset($_SESSION[$key])){
  //     return $_SESSION[$key];
  //   }
  // }



  public static function destroy()
  {
    // unset($_SESSION);
    session_destroy();
  }

}

?>
