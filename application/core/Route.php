<?php


class Route
{
	public static $ch;
	public static function constructor()
	{
		self::$ch = new Guardian();
	}

	public static function Check(){
		self::constructor();
		if(isset($_POST['login']) && isset($_POST['password']))
		{
			$login         =self::$ch->Check('login','post','string');
			$password      =self::$ch->Check('password','post','string');
			if ($login['check'] === 'true'){
				if ($password['check'] === 'true'){
					Session::setData($login,$password);
					return false;
				}
				$m=array('status'=>'"'.$password["var"].'"-Qeyd etdiyiniz məlumat mətn formatında olmalıdır!','code'=>422);
				echo json_encode($m,JSON_UNESCAPED_UNICODE);die();
			}
			$m=array('status'=>'"'.$login["var"].'"-Qeyd etdiyiniz məlumat mətn formatında olmalıdır!','code'=>422);
			echo json_encode($m,JSON_UNESCAPED_UNICODE);die();
		}
		elseif(isset($_POST['login']) && isset($_POST['password']) &&
		isset($_POST['$passwordRe']) && isset($_POST['mail'])){
			$login         =$this->ch->Check('login','post','string');
			$password      =$this->ch->Check('password','post','string');
			$passwordRe      =$this->ch->Check('password','post','string');
			$mail          =$this->ch->Check('mail','post','mail');

			if ($login['check'] === 'true'){
				if ($password['check'] === 'true'){
					if ($passwordRe['check'] === 'true'){
						if ($mail['check'] === 'true'){
							Session::setData($login,$password,$passwordRe,$mail);
							return false;
						}
						$m=array('status'=>'"'.$mail["var"].'"-Qeyd etdiyiniz məlumat mətn formatında olmalıdır!','code'=>422);
						echo json_encode($m,JSON_UNESCAPED_UNICODE); die();
					}
					$m=array('status'=>'"'.$password["var"].'"-Qeyd etdiyiniz məlumat mətn formatında olmalıdır!','code'=>422);
	        echo json_encode($m,JSON_UNESCAPED_UNICODE);die();
				}
				$m=array('status'=>'"'.$password["var"].'"-Qeyd etdiyiniz məlumat mətn formatında olmalıdır!','code'=>422);
        echo json_encode($m,JSON_UNESCAPED_UNICODE);die();
			}
			$m=array('status'=>'"'.$login["var"].'"-Qeyd etdiyiniz məlumat mətn formatında olmalıdır!','code'=>422);
			echo json_encode($m,JSON_UNESCAPED_UNICODE);die();
		}

	}

	static function start()
	{
		if(isset($_POST['login']) && isset($_POST['password'])){
			self::check();
		}

		$controller_name = 'Main';
		$action_name = 'index';
		$routes = explode('/', $_SERVER['REQUEST_URI']);


		if ( !empty($routes[2]) )
		{
			$controller_name = $routes[2];
		}


		if ( !empty($routes[3]) )
		{
			$action_name = $routes[3];
		}


		$model_name = 'Model_'.$controller_name;
		$controller_name = 'Controller_'.$controller_name;
		$action_name = 'action_'.$action_name;

		if($routes[2]=='delete_user'){
			$action_name = 'action_id';
			preg_match('/\id=(.*)/',$routes[3],$matches);
			// print_r($matches);
			// echo $matches[1];
		}
		if($routes[2]=='user_tasks'){
			$action_name = 'action_name';
			preg_match('/name=(.*)/',$routes[3],$matches);
		}
		if($routes[2]=='delete_task'){
			$action_name = 'action_id';
			preg_match('/\id=(.*)/',$routes[3],$matches);
		}



		$controller = new $controller_name;
		$action = $action_name;


		if(method_exists($controller, $action))
		{
			if($action=='action_id' || $action=='action_name'){
				$controller->$action($matches[1]);
			}
			else {
				$controller->$action();
			}
		}
		else
		{

			Route::ErrorPage404();
		}

	}

	function ErrorPage404()
	{
		$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
		header('Location:'.$host.'404');
	}

}
