<?php
session_start();
/**
*FOR ADMIN
*/
class Controller_Picture extends Controller{

  function __construct()
  {
    $this->view = new View();
  }

  public function action_index() {
    if (Session::get('loggedIn') == true){
      $this->view->generate('change_picture_view.php', 'template_view.php','logged_template_view.php');
    }
    elseif (Session::get('admin') == true){
      $this->view->generate('change_picture_view.php', 'template_view.php','admin_menu_view.php');
    }
    else{
      $this->view->generate('error_login_view.php', 'template_view.php');
    }
	}

}

?>
