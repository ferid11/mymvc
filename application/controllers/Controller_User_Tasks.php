<?php
/**
*FOR ADMIN
*/
class Controller_User_Tasks extends Controller{

  function __construct()
  {

    $this->model = new Model_User_List();
    $this->view = new View();
  }

  public function action_name($arg = null) {

    $data = $this->model->userList($arg);
    $this->view->generate('admin_userTaskRoom_view.php', 'template_view.php','admin_menu_view.php',$data);
	}

}

?>
