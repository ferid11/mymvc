<?php
session_start();
/**
*
*/
class Controller_Personal_Archive extends Controller{

  function __construct()
  {
    $this->model = new Model_Personal_Archive();
    // $this->model = new Model_User_List();
    $this->view = new View();
  }

  public function action_index() {
    $data = $this->model->personalArchive();
    // $data = $this->model->userList($arg);
    $this->view->generate('user_personal_archive_view.php', 'template_view.php','logged_template_view.php',$data);
  }

}

?>
