<?php
session_start();
/**
*
*/
class Controller_Suggest extends Controller{

  function __construct()
  {

    $this->model = new Model_Suggest();
    $this->view = new View();
  }


  function action_index()
  {
    $data = $this->model->addSuggest();
    if (Session::get('loggedIn') == true){

      $this->view->generate('suggest_view.php', 'template_view.php','logged_template_view.php');
    }
    elseif(Session::get('admin') == true){
      $this->view->generate('suggest_view.php', 'template_view.php','admin_menu_view.php');
    }
    else{
      $this->view->generate('suggest_view.php', 'template_view.php');
    }
  }

}




?>
