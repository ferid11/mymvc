<?php
/**
*
*/
class Controller_Delete_User extends Controller{

  function __construct()
  {

    $this->model['user_list'] = new Model_User_List();
    $this->model['delete_user'] = new Model_Delete_User();
    $this->view = new View();
  }

  public function action_id($arg = null) {

    $this->model['delete_user']->deleteUser($arg);
    $data = $this->model['user_list']->onlyUsers();
    $this->view->generate('admin_users_view.php', 'template_view.php','admin_menu_view.php',$data);
	}
}


?>
