<?php
/**
*
*/
class Controller_Users extends Controller{

  function __construct()
  {
    $this->model = new Model_User_List();
    // $this->model['TaskCount'] = new Model_Task_Count();
    $this->view = new View();
  }


  function action_index()
  {
    // $data1 = $this->modelTaskCount->getTasksNumber();
    $data = $this->model->onlyUsers();
    $this->view->generate('admin_users_view.php', 'template_view.php','admin_menu_view.php',$data);
  }

}


?>
