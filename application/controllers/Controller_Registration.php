<?php
session_start();
/**
*
*/
class Controller_Registration extends Controller{

  function __construct()
  {

    $this->model = new Model_Registration();
    $this->view = new View();
  }


  function action_index()
  {

    $data = $this->model->getRegistrated();

    if ($data == true){
      // $this->view->generate('user_main_view.php', 'template_view.php','logged_template_view');
      Session::userName();
      header('location: ' .URL.  'main');
    }
    else{
      $this->view->generate('error_registration_view.php', 'template_view.php','logged_template_view');
    }
  }

}


?>
