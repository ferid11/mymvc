<?php
/**
*
*/
class Controller_Delete_Task extends Controller{

  function __construct()
  {

    $this->model['user_list'] = new Model_User_List();
    $this->model['delete_task'] = new Model_Delete_Task();
    $this->view = new View();
  }

  public function action_id($arg = null) {

    $this->model['delete_task']->deleteTask($arg);
    $data = $this->model['user_list']->onlyUsers();
    $this->view->generate('admin_users_view.php', 'template_view.php','admin_menu_view.php',$data);
	}
}


?>
