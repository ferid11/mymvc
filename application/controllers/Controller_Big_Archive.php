<?php
session_start();
/**
*
*/
class Controller_Big_Archive extends Controller{

  function __construct()
  {
    $this->model['bir_archive'] = new Model_Big_Archive();
    $this->view = new View();
  }

  public function action_index() {
    $data = $this->model['bir_archive']->bigArchive();
    // $data = $this->model->userList($arg);
    $this->view->generate('admin_big_archive_view.php', 'template_view.php','admin_menu_view.php',$data);
  }

}

?>
