<?php
session_start();
class Controller_Main extends Controller
{
	function __construct()
  {

    // $this->model = new Model_Login();
    $this->view = new View();
  }
	function action_index()
	{
		// session_destroy();
		// die();
		if (Session::get('admin') == true){
      $this->view->generate('admin_main_view.php', 'template_view.php','admin_menu_view.php');
    }
    elseif (Session::get('loggedIn') == true){
      $this->view->generate('main_view.php', 'template_view.php','logged_template_view.php');
    }
    else{
      $this->view->generate('block_login_view.php', 'template_view.php');
    }


	}
}
