<?php
session_start();
/**
*FOR ADMIN
*/
class Controller_Profile_Pic extends Controller{

  function __construct()
  {

    $this->model = new Model_Profile_Pic();
    $this->view = new View();
  }

  public function action_index() {

    $image = $this->model->uploadPicture();

    if (Session::get('loggedIn') == true){
      $this->view->generate('main_view.php', 'template_view.php','logged_template_view.php');
    }
    elseif (Session::get('admin') == true){
      $this->view->generate('main_view.php', 'template_view.php','admin_menu_view.php');
    }
    else{
      $this->view->generate('error_login_view.php', 'template_view.php');
    }
	}

}

?>
