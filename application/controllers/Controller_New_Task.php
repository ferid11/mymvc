<?php
/**
*
*/
class Controller_New_Task extends Controller{

  function __construct()
  {
    $this->model['user_list'] = new Model_User_List();
    $this->model['send_task'] = new Model_New_Task();
    $this->view = new View();
  }


  function action_index()
  {

    $this->model['send_task']->giveTask();
    // $data = $this->model->addSuggest();
    $data = $this->model['user_list']->onlyUsers();
    $this->view->generate('admin_give_task_view.php', 'template_view.php','admin_menu_view.php',$data);
  }

}


?>
